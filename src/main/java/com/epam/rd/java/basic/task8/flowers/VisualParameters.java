package com.epam.rd.java.basic.task8.flowers;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private AveLenFlower aveLenFlower;

    public String getStemColour () {
        return stemColour;
    }

    public void setStemColour (String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour () {
        return leafColour;
    }

    public void setLeafColour (String leafColour) {
        this.leafColour = leafColour;
    }

    public AveLenFlower getAveLenFlower () {
        return aveLenFlower;
    }

    public void setAveLenFlower (AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public static class AveLenFlower {
        private String measure;
        private String content;

        public String getMeasure () {
            return measure;
        }

        public void setMeasure (String measure) {
            this.measure = measure;
        }

        public String getContent () {
            return content;
        }

        public void setContent (String content) {
            this.content = content;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}

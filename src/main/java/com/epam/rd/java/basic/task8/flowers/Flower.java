package com.epam.rd.java.basic.task8.flowers;

public class Flower {
    private String name;
    private Soil soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private Multiplying multiplying;

    public String getName () {
        return name;
    }

    public void setName (String Name) {
        this.name = Name;
    }

    public String getSoil () {
        return soil.label;
    }

    public void setSoil (String soil) {
        this.soil = Soil.valueOf(soil);
    }

    public String getOrigin () {
        return origin;
    }

    public void setOrigin (String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters () {
        return visualParameters;
    }

    public void setVisualParameters (VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips () {
        return growingTips;
    }

    public void setGrowingTips (GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public String getMultiplying () {
        return multiplying.label;
    }

    public void setMultiplying (String multiplying) {
        this.multiplying = Multiplying.valueOf(multiplying);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public enum Soil {
        PODZOLIC("подзолистая"),
        UNPAVED("грунтовая"),
        SOD_PODZOLIC("дерново-подзолистая");

        public final String label;

        Soil(String label) {
            this.label = label;
        }
    }

    public enum Multiplying {
        LEAVES("листья"),
        CUTTINGS("черенки"),
        SEEDS("семена");

        public final String label;

        Multiplying(String label) {
            this.label = label;
        }
    }


}

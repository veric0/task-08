package com.epam.rd.java.basic.task8.flowers;

public class GrowingTips {
    private Tempreture tempreture;
    private Lighting lighting;
    private Watering watering;

    public Tempreture getTempreture () {
        return tempreture;
    }

    public void setTempreture (Tempreture tempreture) {
        this.tempreture = tempreture;
    }

    public Lighting getLighting () {
        return lighting;
    }

    public void setLighting (Lighting lighting) {
        this.lighting = lighting;
    }

    public Watering getWatering () {
        return watering;
    }

    public void setWatering (Watering watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public static class Tempreture {
        private String measure;
        private String content;

        public String getMeasure() {
            return measure;
        }

        public void setMeasure (String measure) {
            this.measure = measure;
        }

        public String getContent ()
        {
            return content;
        }

        public void setContent (String content) {
            this.content = content;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public enum Lighting {
        YES("yes"),
        NO("no");

        public final String label;

        Lighting(String label) {
            this.label = label;
        }
    }

    public static class Watering {
        private String measure;
        private String content;

        public String getMeasure() {
            return measure;
        }

        public void setMeasure (String measure) {
            this.measure = measure;
        }

        public String getContent ()
        {
            return content;
        }

        public void setContent (String content) {
            this.content = content;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
